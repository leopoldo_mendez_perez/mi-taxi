import { Component, NgZone, OnInit, ViewChildren } from '@angular/core';
import { ViewChild,ElementRef } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Platform } from '@ionic/angular';
import { title } from 'process';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { newArray } from '@angular/compiler/src/util';
import { Geocoder } from '@ionic-native/google-maps';

declare var google;
declare var google:any;
interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  private googleAutocomplete=new google.maps.places.AutocompleteService();
  public searchResult=new Array<any>();
  private originMarker:Marker;
  public destination:any;
  constructor(private geolocation:Geolocation,private ngZone:NgZone) {
    }
  public search:string='';

 map=null;
  addMarkersTopMap(markers){

  }
  ngOnInit() {
    this.loadMap();
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then((geoposition:Geoposition)=>{

      var long= geoposition.coords.longitude;
      var lat= geoposition.coords.latitude;
       // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('map');
    // create LatLng object
    const myLatLng = {lat:lat, lng: long};
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 12
    });
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      const marker={
        position:{
          lat:lat,
          lng:long,
        },
        title:'punto'
      }
      this.addMarker(marker);
    });
    })
 
  }
  addMarker(marker: Marker) {
    return new google.maps.Marker({
      position: marker.position,
      map: this.map,
      title: marker.title
    });
  }
  searchChanged(){
if(!this.search.trim().length) return;
this.googleAutocomplete.getPlacePredictions({input:this.search},predictions=>{
  this.ngZone.run(()=>{
    this.searchResult=predictions;

  });
console.log(this.searchResult);
});
 }
 async calcRoute(item:any){

const info:any=await Geocoder.geocode({address :this.destination.description});
console.log(info);
this.search='';
console.log(item);
this.destination=item;
}
} 