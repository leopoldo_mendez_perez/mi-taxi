import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.page.html',
  styleUrls: ['./login-user.page.scss'],
})
export class LoginUserPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
goToHome(){
this.router.navigate(['/home'])
}
}
