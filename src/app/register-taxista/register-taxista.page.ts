import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-taxista',
  templateUrl: './register-taxista.page.html',
  styleUrls: ['./register-taxista.page.scss'],
})
export class RegisterTaxistaPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  goToHome(){
    this.router.navigate(['/home'])
    }
    user(){
      this.router.navigate(['/register'])
      }
      taxista(){
        this.router.navigate(['/register-taxista'])
        }
}
